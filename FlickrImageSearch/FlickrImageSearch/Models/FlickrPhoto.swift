//
//  FlickrPhoto.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation

struct FlickrPhoto {
    
    let photoId: String?
    let farm: Int?
    let secret: String?
    let server: String?
    let title: String?
    
    var photoUrl:URL {
        var baseURL = "https://farm"
        baseURL = baseURL + "\(farm ?? 0)"
        baseURL = baseURL + ".staticflickr.com/"
        baseURL = baseURL + (server ?? "") + "/"
        var id = photoId ?? ""
        id = id + "_" + (secret ?? "")
        baseURL = baseURL + "\(id).jpg"
        return URL.init(string: baseURL)!
    }
}






