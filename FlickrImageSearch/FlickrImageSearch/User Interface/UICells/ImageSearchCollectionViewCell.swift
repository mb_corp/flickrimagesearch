//
//  ImageSearchCollectionViewCell.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

class ImageSearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView:FlickerImageView!
    
    func configureCell(imageData:FlickrPhoto) {
        self.cellImageView.downloadImageFrom(url: imageData.photoUrl)
        
    }
}

