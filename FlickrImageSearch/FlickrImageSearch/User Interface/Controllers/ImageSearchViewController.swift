//
//  ImageSearchViewController.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

fileprivate let MaxPaginationCount:Int = 50
fileprivate let SectionInset:CGFloat = 5
fileprivate let CellGap:CGFloat = 2

class ImageSearchViewController: UIViewController {
    
    @IBOutlet weak var imageCollectionView:UICollectionView!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var noSearchView:UIView!
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    @IBOutlet weak var resetBtn: UIBarButtonItem!
    
    fileprivate let imageInteractor:ImageSearchInteractor = ImageSearchInteractor()
    fileprivate var initialPaginationCount:Int = 1
    fileprivate var flickerImageData:[FlickrPhoto] = [] {
        didSet {
            if flickerImageData.count > 0 {
                self.resetBtn.isEnabled = false
                self.noSearchView.isHidden = true
            } else {
                self.resetBtn.isEnabled = true
                self.noSearchView.isHidden = false
            }
        }
    }
    fileprivate var searchKey:String = ""
    fileprivate var imageCellWidth:CGFloat = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Flickr Image Search"
        self.imageInteractor.imageSearchDelegate = self
        self.calculateCellSize()
    }
    
    
    @IBAction func resetBtnTapped(_ sender: Any) {
        FlickerImageView.clearCache()
    }
    
    //MARK: Helper's
    
    fileprivate func makeApiCalls() {
        self.imageInteractor.getFlickrImageData(offSet: initialPaginationCount, keyword: self.searchKey)
    }
    
    fileprivate func calculateCellSize() {
        
        let collectionFrame = self.imageCollectionView.frame.size.width - (SectionInset * 2)
        self.imageCellWidth = (collectionFrame/3) - (CellGap * 2)
    }
    
    func resetValues() {
        initialPaginationCount = 1
        self.flickerImageData.removeAll()
        self.imageCollectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let segueType = StoryBoardSegue(rawValue: segue.identifier ?? "") {
            
            switch segueType {
                
            case .imageDetail :
                
                if let viewController = segue.destination as? ImageDetailViewController {
                    viewController.imageData = sender as? FlickrPhoto
                }
                break
            }
        }
    }
}

extension ImageSearchViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.flickerImageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSearchCollectionViewCell", for: indexPath) as? ImageSearchCollectionViewCell {
            cell.configureCell(imageData: self.flickerImageData[indexPath.item])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.item == (self.flickerImageData.count - 1) {
            if initialPaginationCount >= MaxPaginationCount {
                return
            }
            initialPaginationCount = initialPaginationCount + 1
            self.makeApiCalls()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: imageCellWidth, height: imageCellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CellGap
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: SectionInset, left: SectionInset, bottom: SectionInset, right: SectionInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: StoryBoardSegue.imageDetail.rawValue, sender: self.flickerImageData[indexPath.item])
    }
}

extension ImageSearchViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchKey = searchText
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchKey != "" {
            self.resetValues()
            self.makeApiCalls()
            self.spinner.startAnimating()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func cancelSearch() {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        if searchKey == "" {
            self.resetValues()
        }
    }
}

extension ImageSearchViewController : ImageSearchProtocol {
    func didReceivedImages(_ images: [FlickrPhoto]) {
        self.spinner.stopAnimating()
        if images.count <= 0 {
            showAlert(error: "No images found", vc: self)
            return
        }
        self.flickerImageData.append(contentsOf: images)
        self.imageCollectionView.reloadData()
    }
    func didFailToReceiveImages(_ error:String) {
        self.spinner.stopAnimating()
        showAlert(error: error, vc: self)
    }
}

