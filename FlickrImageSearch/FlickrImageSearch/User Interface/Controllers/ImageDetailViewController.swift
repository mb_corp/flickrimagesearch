//
//  ImageDetailViewController.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController {
    
    @IBOutlet weak var detailImageView:FlickerImageView!
    @IBOutlet weak var imageTitle:UILabel!
    
    var imageData:FlickrPhoto?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imgObj = imageData {
            self.detailImageView.downloadImageFrom(url: imgObj.photoUrl)
            self.imageTitle.text = "Image Id: \(self.imageData?.title ?? "")"
        }
    }
}

