//
//  Utils.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation
import  UIKit

public enum StoryBoardSegue:String {
    case imageDetail = "ShowImageDetail"
}

func showAlert(error:String, vc:UIViewController) {
    let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
    let alertVc = UIAlertController(title: "Flickr Search", message: error, preferredStyle: .alert)
    alertVc.addAction(action)
    vc.present(alertVc, animated: true, completion: nil)
}
