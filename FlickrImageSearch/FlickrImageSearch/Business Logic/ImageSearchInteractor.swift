//
//  ImageSearchInteractor.swift
//  FlickrImageSearch
//
//  Created by Mayank on 05/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation

protocol ImageSearchProtocol:class {
    func didReceivedImages(_ images:[FlickrPhoto])
    func didFailToReceiveImages(_ error:String)
}

class ImageSearchInteractor {
    
    //MARK: - iVars
    weak var imageSearchDelegate:ImageSearchProtocol?
    
    
    //MARK: - Network Call's
    func getFlickrImageData(offSet:Int, keyword:String) {
        
        NetworkWrapper.fetchFlickerPhotos(offSet: offSet, keyword: keyword,onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhoto]?) -> Void in
            
            if error != nil {
                self.imageSearchDelegate?.didFailToReceiveImages(error?.localizedDescription ?? "Something went wrong")
                
            } else {
                if let photos = flickrPhotos {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.imageSearchDelegate?.didReceivedImages(photos)
                    })
                    return
                }
            }
            self.imageSearchDelegate?.didFailToReceiveImages("Something went wrong")
        })
    }
}


