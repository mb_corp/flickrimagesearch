//
//  CacheWrapper.swift
//  FlickrImageSearch
//
//  Created by Mayank on 06/12/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation
import  UIKit

fileprivate let imageCache = NSCache<NSString, AnyObject>()

class FlickerImageView: UIImageView {
    
    var imgURL:URL?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 2.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
    }
    
    func downloadImageFrom(url: URL) {
        self.imgURL = url
        imageCache.countLimit = 1000
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) as? UIImage {
            self.image = cachedImage
        } else {
            self.image = #imageLiteral(resourceName: "NoImage_Placeholder")
            self.accessibilityIdentifier = url.absoluteString
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    if let imageToCache = UIImage(data: data) {
                        imageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                        if let cachedImage = imageCache.object(forKey: self.imgURL!.absoluteString as NSString) as? UIImage {
                            self.image = cachedImage
                        } else {
                            print("Fail To Load")
                        }
                    }
                }
                
            }.resume()
        }
    }
    
    class func clearCache() {
        imageCache.removeAllObjects()
    }
}

