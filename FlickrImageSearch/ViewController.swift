//
//  ViewController.swift
//  CrizbuzzDemoApp
//
//  Created by Sunny Agarwal on 2/21/16.
//  Copyright © 2016 Sunny. All rights reserved.
//

import UIKit

let dataSourceURL = NSURL(string:"http://m.cricbuzz.com/interview/newslist")

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var newsTableView: UITableView!
    
    var news = [NewsRecord]()
    let pendingOperations = PendingOperations()
    var deatiledUrl = String()
     var deatiledData = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Sports News"
        fetchPhotoNews()
    }
    
    func fetchPhotoNews() {
        let request = NSURLRequest(URL:dataSourceURL!)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        var task = NSURLSessionDataTask()
        task = session.dataTaskWithRequest(request, completionHandler: {
            (data,response,error) in
            
            if data != nil {
                let datasourceDictionary = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                self.deatiledUrl = datasourceDictionary["detailed_URL"] as! String
                let newsArray = datasourceDictionary["news"] as! [NSMutableDictionary]
                
                for news in newsArray {
                    let id = news["id"] as? String
                    let imageUrl = NSURL(string:news["image"] as? String ?? "")
                    let headLines = news["headline"] as! String
                    if id != nil && imageUrl != nil {
                        let photoRecord = NewsRecord(id:id!, url:imageUrl!,headlines: headLines)
                        self.news.append(photoRecord)
                    }
                }
                
               
            }
            
            if error != nil {
                let alert = UIAlertView(title:"Oops!",message:error!.localizedDescription, delegate:nil, cancelButtonTitle:"OK")
                alert.show()
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                 self.newsTableView.reloadData()
                  UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            })
         
            
        })
        
        task.resume()
        

    }

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
        {
            return 162.0
        }
        return 112.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let newcell = newsTableView.dequeueReusableCellWithIdentifier("newsItem") as! NewsItemTableViewCell

        

        let newsDetails = news[indexPath.row]
        
     
        newcell.newsHeadline?.text = newsDetails.headLines
        newcell.newsImage.image = newsDetails.image
        
     
        switch (newsDetails.state){
        case .Failed:
            newcell.activityIndicator.stopAnimating()
            newcell.newsHeadline?.text = "Failed to load"
            newcell.activityIndicator.hidden = true
        case .Downloaded :
    
            newcell.activityIndicator.stopAnimating()
            newcell.activityIndicator.hidden = true
        case .New:
            newcell.activityIndicator.startAnimating()
            self.startDownloadForRecord(newsDetails,indexPath:indexPath)
        }
        
        return newcell
    }
    
    func startDownloadForRecord(newsDetails: NewsRecord, indexPath: NSIndexPath){

        let downloader = ImageDownloader(newsRecords: newsDetails)
        

     
        
        downloader.completionBlock = {
            if downloader.cancelled {
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.newsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            })
            
            
            
        }

        pendingOperations.downloadQueue.addOperation(downloader)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let id = news[indexPath.row].id
        let detailedUrl = NSURL(string: "\(deatiledUrl)\(id)")
        fetchDetailedNews(detailedUrl!)
       
      
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchDetailedNews(url : NSURL)
    {
        let request = NSURLRequest(URL:url)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        var task = NSURLSessionDataTask()
        task = session.dataTaskWithRequest(request, completionHandler: {
            (data,response,error) in
            
            
          //  dispatch_async(dispatch_get_main_queue(), {
        let datasourceDictionary = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSMutableDictionary
            self.deatiledData = datasourceDictionary
            dispatch_async(dispatch_get_main_queue(), {
                let detailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("detailView") as! DetailedViewController
                print("The data is \(self.deatiledData)")
                detailViewController.dictionary = self.deatiledData
                self.navigationController?.pushViewController(detailViewController, animated: true)
            })
        
            //})
        })
        task.resume()
    }

}

