//
//  ImageDownloadController.swift
//  CrizbuzzDemoApp
//
//  Created by Sunny Agarwal on 2/21/16.
//  Copyright © 2016 Sunny. All rights reserved.
//

import Foundation
import UIKit


enum ImageState {
    case New, Downloaded, Failed
}

class NewsRecord {
    let id:String
    let headLines : String
    let url:NSURL
    var state = ImageState.New
    var image = UIImage(named: "abc")
    
    init(id:String, url:NSURL,headlines : String) {
        self.id = id
        self.url = url
        self.headLines = headlines
    }
}

class PendingOperations {

    lazy var downloadQueue:NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Download queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
}

class ImageDownloader: NSOperation {


    let newsRecord: NewsRecord

    init(newsRecords: NewsRecord) {
        self.newsRecord = newsRecords
    }
    

    override func main() {

        if self.cancelled {
            return
        }

        let imageData = NSData(contentsOfURL:self.newsRecord.url)
        
     
        if self.cancelled {
            return
        }
        
      
        if imageData?.length > 0 {
            self.newsRecord.image = UIImage(data:imageData!)
            self.newsRecord.state = .Downloaded
        }
        else
        {
            self.newsRecord.state = .Failed
            self.newsRecord.image = UIImage(named: "Failed")
        }
    }
    
}

